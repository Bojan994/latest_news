<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    /**
     * Return all news
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $query = News::query();
        $query->orderBy('release_time', 'DESC');

        $perPage = 5;
        $page = $request->input('page', 1);
        $total = $query->count();

        $result = $query->offset(($page - 1) * $perPage)->limit($perPage)->get();

        return [
            'data' => $result,
            'total' => ceil($total / $perPage),
        ];
    }

    /**
     * Return single news article
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        return response()->json(News::find($id));
    }
}
