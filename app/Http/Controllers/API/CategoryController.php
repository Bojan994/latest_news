<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\News;

class CategoryController extends Controller
{
    /**
     * Return all categories
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Category::get());
    }

    /**
     * Return news by category
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function show(Request $request, int $id)
    {
        $query = News::query();
        $query->where('category_id', $id);
        $query->orderBy('release_time', 'DESC');

        $perPage = 5;
        $page = $request->input('page', 1);
        $total = $query->count();

        $result = $query->offset(($page - 1) * $perPage)->limit($perPage)->get();

        return [
            'data' => $result,
            'total' => ceil($total / $perPage),
        ];
    }
}
