<?php

namespace App\Http\Controllers\Parsers;

use App\Http\Controllers\Controller;
use Goutte\Client;
use App\Models\News;
use App\Models\Category;
use Carbon\Carbon;

class DanasNewsParserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $httpClient = new Client();
        $response = $httpClient->request('GET', 'https://www.danas.rs/najnovije-vesti/');

        $titles = [];
        $thumbnails = [];
        $descriptions = [];
        $sourceTitles = [];
        $date = [];
        $time = [];

        $response->filter('.article-post-featured-image a img')
            ->each(function ($node) use (&$thumbnails) {
                $thumbnails[] = $node->attr('src');
            });
        $response->filter('.article-post-title a')
            ->each(function ($node) use (&$titles, &$descriptions, &$date, &$time, &$sourceTitles, $httpClient) {
                $titles[] = $node->text();
                $descriptions[] = $httpClient->click($node->link())
                    ->filter('.flex p')->each(function ($node, $i) {
                        return $node->text();
                    });
                $sourceTitles[] = $httpClient->click($node->link())
                    ->filter('.post-category a')->text();
                $time[] = $httpClient->click($node->link())
                    ->filter('.rank-math-schema')->text();
            });

        $data = [];
        foreach ($titles as $key => $title) {
            $releaseTime = json_decode($time[$key], true);

            $data[$key]['title'] = $title;
            $data[$key]['description'] = $descriptions[$key];
            $data[$key]['thumbnail_url'] = $thumbnails[$key];
            $data[$key]['source_title'] = $sourceTitles[$key];
            $data[$key]['release_time'] = Carbon::parse($releaseTime['@graph'][6]['datePublished'])->format('Y-m-d H:i:s');
        }

        $latestFive = array_slice($data, 0, 5);

        foreach ($latestFive as $articles) {
            $content = '';
            foreach ($articles['description'] as $description) {
                $content .= $description;
            }

            $newsExist = News::where('title', $articles['title'])
                ->first();

            if (!$newsExist) {
                $categoryExist = Category::where('name', $articles['source_title'])
                    ->first();

                $categoryId = $categoryExist->id ?? null;

                if (!$categoryExist) {
                    $category = Category::create([
                        'name' => $articles['source_title'],
                    ]);

                    $categoryId = $category->id;
                }

                News::create([
                    'title' => $articles['title'],
                    'description' => $content,
                    'thumbnail_url' => $articles['thumbnail_url'],
                    'release_time' => $articles['release_time'],
                    'category_id' => $categoryId,
                ]);
            }
        }
    }
}
