<?php

namespace App\Http\Controllers\Parsers;

use App\Http\Controllers\Controller;
use Goutte\Client;
use App\Models\News;
use App\Models\Category;
use Carbon\Carbon;

class N1NewsParserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $httpClient = new Client();
        $response = $httpClient->request('GET', 'https://rs.n1info.com/najnovije/');

        $titles = [];
        $thumbnails = [];
        $descriptions = [];
        $sourceTitles = [];
        $date = [];
        $time = [];

        $response->filter('article div a img')
            ->each(function ($node) use (&$thumbnails) {
                $thumbnails[] = $node->attr('src');
            });
        $response->filter('article div header h4 a')
            ->each(function ($node) use (&$titles, &$descriptions, &$date, &$time, &$sourceTitles, $httpClient) {
                $titles[] = $node->text();
                $descriptions[] = $httpClient->click($node->link())
                    ->filter('.entry-content p')->each(function ($node, $i) {
                        return $node->text();
                    });
                $sourceTitles[] = $httpClient->click($node->link())
                    ->filter('.cat-name a')->text();
                $date[] = $httpClient->click($node->link())
                    ->filter('.meta div .post-time span')->text();
                $time[] = $httpClient->click($node->link())
                    ->filter('.meta div .post-time span')->eq(1)->text();
            });

        $data = [];
        foreach ($titles as $key => $title) {
            $data[$key]['title'] = $title;
            $data[$key]['description'] = $descriptions[$key];
            $data[$key]['thumbnail_url'] = $thumbnails[$key];
            $data[$key]['source_title'] = $sourceTitles[$key];
            $data[$key]['release_time']['date'] = Carbon::parse($date[$key])->format('Y-m-d');
            $data[$key]['release_time']['time'] = Carbon::parse(substr($time[$key], 0, 5))->format('H:i:s');
        }

        $latestFive = array_slice($data, 0, 5);

        foreach ($latestFive as $articles) {
            $content = '';
            foreach ($articles['description'] as $description) {
                $content .= $description;
            }

            $newsExist = News::where('title', $articles['title'])
                ->first();

            if (!$newsExist) {
                $categoryExist = Category::where('name', $articles['source_title'])
                    ->first();

                $categoryId = $categoryExist->id ?? null;

                if (!$categoryExist) {
                    $category = Category::create([
                        'name' => $articles['source_title'],
                    ]);

                    $categoryId = $category->id;
                }

                News::create([
                    'title' => $articles['title'],
                    'description' => $content,
                    'thumbnail_url' => $articles['thumbnail_url'],
                    'release_time' => $articles['release_time']['date'] . ' ' . $articles['release_time']['time'],
                    'category_id' => $categoryId,
                ]);
            }
        }
    }
}
